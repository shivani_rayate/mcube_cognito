const User = require('../api/models/user');


class UserCls {
    UserCls() { }

    findUserByEmail(params) {
        return new Promise((resolve, reject) => {
            User.findOne({ email: params.email }).exec((err, data) => {
                if (err) {
                    console.error(`Error :: findUserByEmail > findOne has error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    findAll() {
        return new Promise((resolve, reject) => {
            User.find({})
                .sort({ _id: -1 })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        console.error(`User Mongo Find all data > :: ${JSON.stringify(data)}`);
                        resolve(data);
                    }
                });
        });
    }

    findUserByid(params) {
        return new Promise((resolve, reject) => {
            User.findById({ _id: params.id }).exec((err, data) => {
                if (err) {
                    console.error(`Error :: findUserByid :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    save(params) {
        const userDocument = new User({
            id : params.id ? params.id :0,
            fullname : params.fullname ? params.fullname : null,
            email : params.email ? params.email : null,
            password : params.password ? params.password : null,
            //user_id : UserId,
            // email: params.email ? params.email : null,
            // password: params.password ? params.password : null,
            // gender: params.gender ? params.gender : null,
            // first_name: params.first_name ? params.first_name : null,
            // last_name: params.last_name ? params.last_name : null,
            // phone: params.phone ? params.phone : null,
            user_id: params.userId,
            user_type: 'user',
            // content_provider: params.content_provider,
            // publisher_admin: params.publisher_admin
        });

        console.log(`USER SAVE BODY PARAMS :: ${JSON.stringify(params)}`);

        return new Promise((resolve, reject) => {
            userDocument.save((err, data) => {
                if (err) {
                    console.error(`Error :: Mongo Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        code: 200,
                        message: data,
                    };
                    resolve(response);
                }
            });
        });
    }


   
    authenticateUser(params) {
        // console.log('params.username: ' + params.username + ' params.password: ' + params.password)
        return new Promise((resolve, reject) => {
            const obj = {};
            User.findOne({ email: params.username })
                .exec((err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log(`USER DAATA : ${JSON.stringify(data)}`);
                        obj.status = 200;
                        obj.data = data;
                        resolve(obj);
                    }
                });
        });
    }
}

module.exports = {
    UserClass: UserCls,
};