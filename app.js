const express = require('express');
const app = express();
const router = express.Router();
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const chalk = require('chalk');
const session = require('express-session');
const logger = require('morgan');  // To log Incoming Request // it generates logs automatically.
const mongoose = require('mongoose');
const compression = require('compression');
const helmet = require('helmet');
const cors = require('cors');

const mongoDb = require('./config/mongoDb');

// connect to mongoDB
mongoDb.getMongoDbs().then((dbs) => {
  for (let db of dbs) {
    mongoDb.connectMongoDb(db.name)
  }
})


CognitoExpress = require("cognito-express");

// req for middleware
var check_auth = require('./api/middleware/check-auth')



/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 3003);


// For CROS ERROR
app.use((req, res, next) => {
  res.header("Access-control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
    return res.status(200).json({});
  }
  next();
});


// Routes Which should Handle Request
const index = require('./api/routes/index');
const users = require('./api/routes/users');
const config = require('./api/routes/config');


app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
  limit: '26000mb',
  extended: true,
}));
app.use(bodyParser.json({ limit: '26000mb' }));

app.use(logger('dev'));


app.use(session({
  secret: 'dam-api-session-parameter',
  resave: true,
  saveUninitialized: true
}));


app.use('/api', router);
app.use('/index', index);
app.use('/config', config);


app.use('/api/users', check_auth, users);
app.use(cookieParser());


// helmet for security purpose
app.use(helmet());

// CORS - To hanlde cross origin requests
app.use(cors());


//compress all responses to Make App Faster
app.use(compression());


/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
  console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
  console.log('  Press CTRL-C to stop\n');
});

module.exports = app;