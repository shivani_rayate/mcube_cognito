const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool;
const AWS = require('aws-sdk');
const request = require('request');
const jwkToPem = require('jwk-to-pem');
const jwt = require('jsonwebtoken');
global.fetch = require('node-fetch');

// ####################################################

global.navigator = () => null;  // VIMP SOLUTION on >>>  navigator not defined error
// ####################################################

const awsConfig = require('../../config/aws_config')['development'];
const tenantConfig = require('../../config/tenantConfig.json');


AWS.config.update({
  accessKeyId: awsConfig.accessKeyId,
  secretAccessKey: awsConfig.secretAccessKey,
  region: awsConfig.region,
});

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();


// Authenticate and Set a New Password for a User Created with the AdminCreateUser
exports.AuthenticateUser = (paramsReq) => {

  return new Promise((resolve, reject) => {

    var params = {
      poolId: paramsReq.poolId,
      username: paramsReq.username,
      password: paramsReq.password,
      newPassword: paramsReq.newPassword
    }

    console.log('AuthenticateUser params >>>>>> ' + JSON.stringify(params))

    var authenticationData = {
      Username: params.username,
      Password: params.password,
    };

    const poolData = {
      UserPoolId: params.poolId, // Your user pool id here    
      ClientId: tenantConfig[params.poolId].config.clientId // Your client id here
    };
    console.log('AuthenticateUser poolData >>>>>> ' + JSON.stringify(poolData))

    const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
    var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);

    var userData = {
      Username: params.username,
      Pool: userPool
    };

    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

    var attributesData = {
      email: params.username
    }

    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function (result) {
        // User authentication was successful
        console.log("######## onSuccess #########") //
        console.log('access token + ' + result.getAccessToken().getJwtToken());
        console.log('idToken + ' + result.idToken.jwtToken);// User authentication was successful

        let obj = {};
        obj.status = 200;
        obj.message = 'User Authentication Successed';
        obj.data = result;
        resolve(obj)
      },

      onFailure: function (err) {
        console.log("######## onFailure #########") //
        console.log(err) // User authentication was not successful
        let obj = {};
        obj.status = 403;
        obj.message = err.message;
        obj.data = null;
        resolve(obj)
      },

      newPasswordRequired: function (userAttributes, requiredAttributes) {
        console.log("######## newPasswordRequired #########") //
        cognitoUser.completeNewPasswordChallenge(params.newPassword, attributesData, this)

      }
    })
  })
}


// RegisterUser
exports.RegisterUser = (paramsReq) => {
  return new Promise((resolve, reject) => {

    var params = {
      UserPoolId: paramsReq.UserPoolId, /* required */
      Username: paramsReq.email, /* required */
      TemporaryPassword: paramsReq.password,
      DesiredDeliveryMediums: [
        'EMAIL'
      ],
      ForceAliasCreation: false,
      // MessageAction: 'SUPPRESS',
      UserAttributes: [
        {
          Name: 'email', /* required */
          Value: paramsReq.email
        },
        {
          Name: 'name', /* required */
          Value: paramsReq.name
        },
        {
          Name: 'email_verified', /* required */
          Value: 'true' // needed to set email_verified attribute to true to make reset password flow to work.
        },
        /* more items */
      ]
    };

    cognitoidentityserviceprovider.adminCreateUser(params, function (err, data) {
      if (err) {
        console.log(err, err.stack); // an error occurred
        reject()
      } else {
        console.log('user data is ' + JSON.stringify(data));
        resolve(data)
      }
    });
  })
};


// ConfirmSignUp
exports.confirmSignUp = (paramsReq) => {
  var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
  return new Promise((resolve, reject) => {
    var params = {
      ClientId: awsConfig.COGNITO.ClientId, /* required */
      ConfirmationCode: paramsReq.ConfirmationCode, /* required */
      Username: paramsReq.Username, /* required */

      // AnalyticsMetadata: {
      //     AnalyticsEndpointId: 'STRING_VALUE'
      // },
      // ClientMetadata: {
      // },
      // ForceAliasCreation: true || false,
      // SecretHash: 'STRING_VALUE',
      // UserContextData: {
      //     EncodedData: 'STRING_VALUE'
      // }

    };
    cognitoidentityserviceprovider.confirmSignUp(params, function (err, data) {
      if (err) {
        console.log(err, err.stack); // an error occurred
        reject()
      }
      else {
        console.log(data);           // successful response
        resolve(data)
      }
    });
  })

};


// GetUsers
exports.getUsers = (paramsReq) => {

  var params = {
    UserPoolId: paramsReq.UserPoolId, // Your user pool id here 
    AttributesToGet: [
      'name'
    ],
  };

  return new Promise((resolve, reject) => {
    var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.listUsers(params, (err, data) => {
      if (err) {
        console.log(err);
        reject(err)
      }
      else {
        //console.log("getUsers data from cognito >> ", JSON.stringify(data));
        resolve(data)
      }
    })
  })
};


// Login
exports.Login = (paramsReq) => {
  return new Promise((resolve, reject) => {

    const poolData = {
      UserPoolId: paramsReq.poolId, // Your user pool id here    
      ClientId: tenantConfig[paramsReq.poolId].config.clientId // Your client id here
    };
    const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

    var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
      Username: paramsReq.username,
      Password: paramsReq.password,
    });
    var userData = {
      Username: paramsReq.username,
      Pool: userPool
    };
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function (result) {

        // console.log('result >>>  + ' + JSON.stringify(result));
        // console.log('access token + ' + result.getAccessToken().getJwtToken());
        // console.log('id token + ' + result.getIdToken().getJwtToken());
        // console.log('refresh token + ' + result.getRefreshToken().getToken());
        resolve({
          status: 200,
          message: 'Login successful',
          data: result
        });

      },
      onFailure: function (err) {
        console.log(`\n Login > onFailure > ` + err, err.stack);

        if (err.message === 'Only radix 2, 4, 8, 16, 32 are supported') {
          err.message = 'Incorrect username or password.'
        }

        resolve({
          status: 403,
          message: err.message,
          data: null
        });
      },

    });
  })
};


// Delete User
exports.deleteUser = (paramsReq) => {
  console.log("delete user params > " + JSON.stringify(paramsReq))
  return new Promise((resolve, reject) => {

    var cognitoUser = {
      UserPoolId: paramsReq.UserPoolId, // Your user pool id here 
      Username: paramsReq.username
    }
    console.log("delete user params > " + JSON.stringify(cognitoUser))
    cognitoidentityserviceprovider.adminDeleteUser(cognitoUser, function (err, result) {
      if (err) {
        console.log(err);
        reject(err)
      } else {
        console.log("deleteUser from cognito >> ", JSON.stringify(result));
        resolve(result)
      }

    });

  })
}


// getUserInfo
exports.getUserInfo = (paramsReq) => {
  return new Promise((resolve, reject) => {
    var cognitoUser = {
      UserPoolId: paramsReq.UserPoolId, // Your user pool id here 
      Username: paramsReq.username
    }

    cognitoidentityserviceprovider.adminGetUser(cognitoUser, function (err, result) {
      if (err) {
        console.log(err);
        reject(err)
      } else {
        //console.log("getUserInfo from cognito >> ", JSON.stringify(result));
        resolve(result)
      }

    });
  })
}


// disableUser
exports.disableUser = (paramsReq) => {
  return new Promise((resolve, reject) => {

    var cognitoUser = {
      UserPoolId: paramsReq.UserPoolId, // Your user pool id here 
      Username: paramsReq.username
    }
    console.log("disable user > " + JSON.stringify(cognitoUser))

    cognitoidentityserviceprovider.adminDisableUser(cognitoUser, function (err, result) {
      if (err) {
        console.log(err);
        reject(err)
      } else {
        console.log("disableUser from cognito >> ", JSON.stringify(result));
        resolve(result)
      }

    });

  })
}


// enableUser
exports.enableUser = (paramsReq) => {
  return new Promise((resolve, reject) => {

    var cognitoUser = {
      UserPoolId: paramsReq.UserPoolId, // Your user pool id here 
      Username: paramsReq.username
    }

    cognitoidentityserviceprovider.adminEnableUser(cognitoUser, function (err, result) {
      if (err) {
        console.log(err);
        reject(err)
      } else {
        console.log("enableUser from cognito >> ", JSON.stringify(result));
        resolve(result)
      }

    });

  })
}


// admin Add User To Group
exports.addUserToGroup = (paramsReq) => {
  return new Promise((resolve, reject) => {
    var cognitoUser = {
      GroupName: paramsReq.groupname,
      UserPoolId: paramsReq.UserPoolId, // Your user pool id here
      Username: paramsReq.username
    }
    //console.log("cognitoUser >> ", JSON.stringify(cognitoUser));
    cognitoidentityserviceprovider.adminAddUserToGroup(cognitoUser, function (err, result) {
      if (err) {
        console.log(err);
        reject(err)
      } else {
        //console.log("addUser To Group >> ", JSON.stringify(result));
        resolve(result)
      }

    });
  })
}

// create Group
exports.createGroup = (paramsReq) => {
  return new Promise((resolve, reject) => {
    var params = {
      GroupName: paramsReq.groupname,
      UserPoolId: paramsReq.UserPoolId,
      // Description: 'STRING_VALUE',
      // Precedence: 'NUMBER_VALUE',
      // RoleArn: 'STRING_VALUE'
    }

    console.log("params >> ", JSON.stringify(params));
    cognitoidentityserviceprovider.createGroup(params, function (err, result) {
      if (err) {
        console.log(err);
        reject(err)
      } else {
        console.log("Create Group >> ", JSON.stringify(result));
        resolve(result)
      }

    });
  })
}


// get list of groups
exports.getListOfGroups = (paramsReq) => {
  return new Promise((resolve, reject) => {
    var cognitoUser = {

      UserPoolId: paramsReq.UserPoolId,
      Limit: '10',
      // NextToken: ''
    }
    console.log("cognitoUser >> ", JSON.stringify(cognitoUser));
    cognitoidentityserviceprovider.listGroups(cognitoUser, function (err, result) {
      if (err) {
        console.log(err);
        reject(err)
      } else {
        // console.log("listGroups >> ", JSON.stringify(result));
        resolve(result)
      }

    });


  })
}


// delete groups
exports.deleteGroup = (paramsReq) => {
  return new Promise((resolve, reject) => {
    var params = {

      UserPoolId: paramsReq.UserPoolId,
      GroupName: paramsReq.groupname

    }
    //console.log("params >> ", JSON.stringify(params));
    cognitoidentityserviceprovider.deleteGroup(params, function (err, result) {
      if (err) {
        console.log(err);
        reject(err)
      } else {
        console.log("delete Group from cognito>> ", JSON.stringify(result));
        resolve(result)
      }

    });

  })
}


// change password
exports.changePassword = (paramsReq) => {
  return new Promise((resolve, reject) => {
    var params = {
      AccessToken: paramsReq.accesstoken,
      PreviousPassword: paramsReq.currentpassword,
      ProposedPassword: paramsReq.newpassword
    }
    cognitoidentityserviceprovider.changePassword(params, function (err, result) {
      if (err) {
        console.log("changePassword ERR >> " + err);
        let obj = {};
        obj.status = err.statusCode;
        obj.message = err.message;
        obj.data = null;
        reject(obj)
      } else {
        let obj = {};
        obj.status = 200;
        obj.message = 'Your account password has been Successfully changed.Please login with your new password';
        obj.data = result;
        resolve(obj)
      }
    })
  })
}

// Update User Attributes

exports.updateUserAttributes = (paramsReq) => {
  return new Promise((resolve, reject) => {
    var params = {
      AccessToken: paramsReq.accesstoken,
      UserAttributes: paramsReq.UserAttributes
    }
    console.log("update params data in cognito > " + JSON.stringify(params))
    cognitoidentityserviceprovider.updateUserAttributes(params, function (err, result) {
      if (err) {
        console.log(err);
        reject(err)
      } else {
        console.log("update from cognito >> ", JSON.stringify(result));
        resolve(result)
      }

    });

  })
}


// getGroupInfo
exports.getGroupInfo = (paramsReq) => {
  return new Promise((resolve, reject) => {
    var params = {
      UserPoolId: paramsReq.UserPoolId, // Your user pool id here 
      GroupName: paramsReq.groupname
    }

    cognitoidentityserviceprovider.getGroup(params, function (err, result) {
      if (err) {
        console.log(err);
        reject(err)
      } else {
        //console.log("getGroupInfo from cognito >> ", JSON.stringify(result));
        resolve(result)
      }

    });
  })
}



//get ListUsersInGroup

exports.listUsersInGroup = (paramsReq) => {
  return new Promise((resolve, reject) => {
    var params = {

      UserPoolId: paramsReq.UserPoolId,
      GroupName: paramsReq.groupname,
      Limit: '10',
      // NextToken: "string",

    }
    console.log("params >> ", JSON.stringify(params));
    cognitoidentityserviceprovider.listUsersInGroup(params, function (err, result) {
      if (err) {
        console.log(err);
        reject(err)
      } else {
        console.log("ListUsersInGroup >> ", JSON.stringify(result));
        resolve(result)
      }

    });


  })
}



// delete User From Group
exports.adminRemoveUserFromGroup = (paramsReq) => {
  return new Promise((resolve, reject) => {
    var params = {

      UserPoolId: paramsReq.UserPoolId,
      GroupName: paramsReq.groupname,
      Username: paramsReq.username

    }
    console.log("params in cognito  >> ", JSON.stringify(params));
    cognitoidentityserviceprovider.adminRemoveUserFromGroup(params, function (err, result) {
      if (err) {
        console.log(err);
        reject(err)
      } else {
        console.log("Removed User from Groups of cognito>> ", JSON.stringify(result));
        resolve(result)
      }

    });

  })
}


// forgot password
exports.ForgotPassword = (paramsReq) => {
  return new Promise((resolve, reject) => {
    var params = {
      ClientId: tenantConfig[paramsReq.poolId].config.clientId, // Your client id here
      Username: paramsReq.username
    }
    console.log("params in ForgotPassword  >> ", JSON.stringify(params));
    cognitoidentityserviceprovider.forgotPassword(params, function (err, result) {
      if (err) {
        console.log(err);
        reject(err)
      } else {
        console.log("ForgotPassword Success >> ", JSON.stringify(result));
        resolve(result);
      }
    });
  })
}


// reset password
exports.ConfirmForgotPassword = (paramsReq) => {
  return new Promise((resolve, reject) => {
    var params = {
      ClientId: tenantConfig[paramsReq.poolId].config.clientId,
      ConfirmationCode: paramsReq.ConfirmationCode,
      Password: paramsReq.Password,
      Username: paramsReq.Username,
    }
    console.log("params in ConfirmForgotPassword  >> ", JSON.stringify(params));
    cognitoidentityserviceprovider.confirmForgotPassword(params, function (err, result) {
      if (err) {
        console.log(err);
        reject(err)
      } else {
        console.log("ConfirmForgotPassword Success >> ", JSON.stringify(result));
        resolve(result);
      }
    });
  })
}