const express = require('express');
const router = express.Router();
const AWS = require('aws-sdk');


const cognito = require('../utilities/cognito');

const download = require('download');
const fs = require('fs');

const awsConfig = require('../../config/aws_config')['development'];
AWS.config.update({
  accessKeyId: awsConfig.accessKeyId,
  secretAccessKey: awsConfig.secretAccessKey,
  region: awsConfig.region,
});


// For Login
router.post('/login', (req, res, next) => {
  console.log("\n /login called in API");

  var tenantId = req.body.tenantId;

  // get tenant config from dynamodb first
  var docClient = new AWS.DynamoDB.DocumentClient();

  var _params = {
    TableName: awsConfig.cloudformation.configDynamodbTable,
    Key: {
      'poolName': tenantId
    }
  };

  console.log("\n / I n cognit micro >> " + JSON.stringify(_params));



  const getTenantPromise = docClient.get(_params).promise();

  console.log("\n / I n cognit micro getTenantPromise >> " + JSON.stringify(getTenantPromise));


  getTenantPromise.then(
    (data) => {
      console.log("\n / I n cognit micro getTenantPromise >> " + JSON.stringify(data));


      if (data.Item && data.Item.poolName === tenantId) {

        let params = {
          poolId: data.Item.poolId,
          username: req.body.username,
          password: req.body.password,
        }

        cognito.Login(params).then((response) => {

          if (response.status === 200) {

            res.json({
              status: 200,
              message: 'Login successful',
              data: response.data,
            });

            console.log("\n login data >>> " + JSON.stringify(response.data))
            console.log("\n login data token >>> " + JSON.stringify(response.data.idToken.jwtToken))

          } else {

            res.json({
              status: 403,
              message: response.message,
              data: response.data,
            })
          }
        }).catch((err) => {
          res.json({
            status: 500,
            message: 'Oops ! Some error occured, please try again later.',
            data: null,
          });
        });
      } else {
        res.json({
          status: 403,
          message: "Incorrect Tenant ID or Alias",
          data: null
        })
      }
    },
    (err) => {
      console.error("\n Unable to scan the tenantInfo table. Error JSON:", JSON.stringify(err, null, 2));

      res.json({
        status: 500,
        message: 'Oops ! Some error occured, please try again later.',
        data: null,
      });
    }
  );
})


// authenticateUser with AWS Cognito
router.post('/authenticateUser', (req, res, next) => {
  console.log("authenticateUser with AWS Cognito called");

  var tenantId = req.body.tenantId;

  // get tenant config from dynamodb first
  var docClient = new AWS.DynamoDB.DocumentClient();

  var _params = {
    TableName: awsConfig.cloudformation.configDynamodbTable,
    Key: {
      'poolName': tenantId
    }
  };

  const getTenantPromise = docClient.get(_params).promise();

  getTenantPromise.then(
    (data) => {
      if (data.Item && data.Item.poolName === tenantId) {

        let params = {
          poolId: data.Item.poolId,
          username: req.body.username,
          password: req.body.password,
          newPassword: req.body.newPassword
        }

        cognito.AuthenticateUser(params).then((response) => {
          if (response.status === 200) {
            console.log('user sign in response >> ' + JSON.stringify(response));
            res.json({
              status: 200,
              message: 'User Signed Successfuly',
              data: response.data,
            })
          } else {
            res.json({
              status: 403,
              message: response.message,
              data: response.data,
            })
          }
        }).catch((err) => {
          console.log(`error IN USER sign in : ${err}`);
          res.json({
            status: 500,
            message: 'Oops ! Some error occured, please try again later.',
            data: null,
          });
        });

      } else {
        res.json({
          status: 403,
          message: "Incorrect Tenant ID",
          data: null
        })
      }

    },
    (err) => {
      console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));

      res.json({
        status: 500,
        message: 'Oops ! Some error occured, please try again later.',
        data: null,
      });
    }
  );
})


// Forgot Password
router.post('/forgotPassword', (req, res, next) => {
  console.log("forgotPassword API called");

  let tenantId = req.body.tenantId;

  // get tenant config from dynamodb first
  var docClient = new AWS.DynamoDB.DocumentClient();

  var _params = {
    TableName: awsConfig.cloudformation.configDynamodbTable,
    Key: {
      'poolName': tenantId
    }
  };

  const getTenantPromise = docClient.get(_params).promise();

  getTenantPromise.then(
    (data) => {
      if (data.Item && data.Item.poolName === tenantId) {

        let params = {
          poolId: data.Item.poolId,
          username: req.body.username
        }

        cognito.ForgotPassword(params).then((response) => {
          console.log('ForgotPassword response >> ' + JSON.stringify(response));

          res.json({
            status: 200,
            message: 'ForgotPassword Success',
            data: response
          });

        }).catch((err) => {
          console.log(`error IN ForgotPassword : ${err}`);
          res.json({
            status: 500,
            message: err.message,
            data: null,
          });
        });

      } else {
        res.json({
          status: 403,
          message: "Incorrect Tenant ID",
          data: null
        })
      }
    },
    (err) => {
      console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));

      res.json({
        status: 500,
        message: 'Oops ! Some error occured, please try again later.',
        data: null,
      });
    }
  );
})


// Reset Password
router.post('/resetPassword', (req, res, next) => {
  console.log("resetPassword API called");

  let tenantId = req.body.tenantId;

  // get tenant config from dynamodb first
  var docClient = new AWS.DynamoDB.DocumentClient();

  var _params = {
    TableName: awsConfig.cloudformation.configDynamodbTable,
    Key: {
      'poolName': tenantId
    }
  };

  const getTenantPromise = docClient.get(_params).promise();

  getTenantPromise.then(
    (data) => {
      if (data.Item && data.Item.poolName === tenantId) {

        let params = {
          poolId: data.Item.poolId,
          ConfirmationCode: req.body.confirmationCode,
          Password: req.body.password,
          Username: req.body.username
        }

        cognito.ConfirmForgotPassword(params).then((response) => {
          console.log('resetPassword response >> ' + JSON.stringify(response));

          res.json({
            status: 200,
            message: 'resetPassword Success',
            data: response
          });

        }).catch((err) => {
          console.log(`error IN resetPassword : ${err}`);
          res.json({
            status: 500,
            message: err.message,
            data: null,
          });
        });

      } else {
        res.json({
          status: 403,
          message: "Incorrect Tenant ID",
          data: null
        })
      }
    },
    (err) => {
      console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));

      res.json({
        status: 500,
        message: 'Oops ! Some error occured, please try again later.',
        data: null,
      });
    }
  );

})



// ########################################################################3

router.get('/downloadFile', (req, res) => {

  console.log('Download called >>');

  var url = req.query.url;
  var options = {
    filename: 'package.mp4'
  }

  download(url, 'download', options).then((data) => { // (url, directory, options)

    res.download('download/package.mp4', function (err) {
      if (err) throw err
      console.log("Downloaded!")
      fs.unlink('download/package.mp4', function (err) {
        if (err) throw err;
        console.log('File deleted!');

      });
    })
  })

})


module.exports = router;