var express = require('express');
var router = express.Router();

//const checkAuth = require('../middleware/check-auth');

const Users = require("../../worker/users");
const usersObj = new Users.UserClass();

const Cognito_controller = require('../utilities/cognito');

const tenantConfig = require('../../config/tenantConfig');


// confirmSignUp
router.post('/confirmSignUp', function (req, res, next) {
  console.log("confirmSignUp POST called")
  const params = {
    ConfirmationCode: req.body.ConfirmationCode ? req.body.ConfirmationCode : null,
    Username: req.body.Username ? req.body.Username : null,
  }
  Cognito_controller.confirmSignUp(params).then((response) => {
    if (response) {
      console.log('confirmSignUp response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Signup Confirmed',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER Signup Confirmation: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
});


// registration
router.post('/register', (req, res, next) => {

  console.log("register POST called")

  let tenantId = res.locals.tenantId;

  const params = {
    UserPoolId: tenantId,
    name: req.body.name ? req.body.name : null,
    email: req.body.email ? req.body.email : null,
    password: req.body.password ? req.body.password : null
  };

  Cognito_controller.RegisterUser(params).then((response) => {
    if (response) {
      console.log('user data saved response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Saved Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER SAVE: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });

});


// group creation
router.post('/createGroup', (req, res, next) => {

  console.log("createGroup POST called")

  let tenantId = res.locals.tenantId;

  const params = {
    UserPoolId: tenantId,
    groupname: req.body.groupname ? req.body.groupname : null
  };

  Cognito_controller.createGroup(params).then((response) => {
    if (response) {
      console.log('createGroup response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'Group Create Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN GROUP CREATION: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });

});


// Get All Users
router.get('/getUsers', (req, res, next) => {
  console.log('\n /getUsers called in mcube_cognito');

  let tenantId = res.locals.tenantId;

  const params = {
    UserPoolId: tenantId
  }

  Cognito_controller.getUsers(params).then((users) => {
    res.json({
      status: 200,
      message: 'Users fetched successfully',
      data: users,
    });
  });

});


// Delete User
router.post('/deleteUser', (req, res, next) => {
  console.log("deleteUser POST called");

  let tenantId = res.locals.tenantId;

  const params = {
    UserPoolId: tenantId,
    username: req.body.username ? req.body.username : null
  };

  Cognito_controller.deleteUser(params).then((response) => {
    if (response) {
      console.log('user delete response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Deleted Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER Delete: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})

// getUserInfo
router.get('/getUserInfo/:username', (req, res, next) => {
  console.log('***getUserInfo CALLED***');

  let tenantId = res.locals.tenantId;

  const params = {
    UserPoolId: tenantId,
    username: req.params.username ? req.params.username : null
  };

  Cognito_controller.getUserInfo(params).then((user) => {
    res.json({
      status: 200,
      message: 'getUserInfo fetched successfully',
      data: user,
    });
  });

})


// disableUser
router.post('/disableUser', (req, res, next) => {

  let tenantId = res.locals.tenantId;

  const params = {
    UserPoolId: tenantId,
    username: req.body.username ? req.body.username : null
  };
  Cognito_controller.disableUser(params).then((response) => {
    if (response) {
      console.log('disableUser response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Disabled Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER Disable: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})


// enableUser
router.post('/enableUser', (req, res, next) => {
  console.log("enableUser POST called")

  let tenantId = res.locals.tenantId;

  const params = {
    UserPoolId: tenantId,
    username: req.body.username ? req.body.username : null
  };

  Cognito_controller.enableUser(params).then((response) => {
    if (response) {
      console.log('enableUser response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Enabled Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER Enable: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})

// Change Password
router.post('/changePassword', (req, res, next) => {
  console.log("ChangePassword POST called")
  const params = {
    accesstoken: req.session.userData ? req.session.userData.accessToken.jwtToken : null,
    currentpassword: req.body.currentpassword ? req.body.currentpassword : null,
    newpassword: req.body.newpassword ? req.body.newpassword : null
  }
  Cognito_controller.changePassword(params).then((response) => {
    if (response) {
      setTimeout(function () {
        req.session.destroy(function (err) {
          if (err) {
            res.redirect('/login');
          } else {
            req.session = null;
          }
        });
      }, 3000)
      res.json({
        status: response.status,
        message: response.message,
        data: response.data,
      })
    }
  }).catch((err) => {
    console.log(`error in Password: ${err}`);
    res.json({
      status: err.status,
      message: err.message,
      data: err.data,
    })
  });
})


// Update User Attributes

router.post('/updateUserAttributes', (req, res, next) => {
  console.log("Update User Attribute  POST called")
  const params = {
    accesstoken: req.session.userData ? req.session.userData.accessToken.jwtToken : null,
    UserAttributes: req.body.UserAttributes ? req.body.UserAttributes : null,
  }
  console.log("update user attributes > " + JSON.stringify(params))
  Cognito_controller.updateUserAttributes(params).then((response) => {
    if (response) {
      console.log('update user response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'Update Successfully',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });

})


// add User To Group
router.post('/addUserToGroup', (req, res, next) => {
  console.log("addUserToGroup POST called")

  let tenantId = res.locals.tenantId;

  const params = {
    UserPoolId: tenantId,
    username: req.body.username ? req.body.username : null,
    groupname: req.body.groupname ? req.body.groupname : null
  };
  //console.log(JSON.stringify(params))

  Cognito_controller.addUserToGroup(params).then((response) => {
    if (response) {
      console.log('addUserToGroup response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Added To Group Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN addUserToGroup: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})





// Create Group
router.post('/createGroup', (req, res, next) => {
  console.log("createGroup POST called")
  const params = {
    groupname: req.body.groupname ? req.body.groupname : null

  };

  console.log(JSON.stringify(params))

  Cognito_controller.addUserToGroup(params).then((response) => {
    if (response) {
      console.log('createGroup response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'Group Created Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN Group Creation: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})

// Get All Groups
router.get('/getListOfGroups', (req, res, next) => {
  console.log('***GET getListOfGroups CALLED***');

  let tenantId = res.locals.tenantId;

  const params = {
    UserPoolId: tenantId
  }

  Cognito_controller.getListOfGroups(params).then((groups) => {
    res.json({
      status: 200,
      message: 'List Of Groups fetched successfully',
      data: groups,
    });
  });

});



// Delete User
router.post('/deleteGroup', (req, res, next) => {
  console.log("deleteGroup POST called")

  let tenantId = res.locals.tenantId;

  const params = {
    UserPoolId: tenantId,
    groupname: req.body.groupname ? req.body.groupname : null
  };

  //console.log(JSON.stringify(params))

  Cognito_controller.deleteGroup(params).then((response) => {
    if (response) {
      console.log('group delete response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'Group Deleted Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN Group Delete: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})



// getGroupInfo
router.get('/getGroupInfo/:groupname', (req, res, next) => {
  console.log('***getGroupInfo CALLED***');

  const params = {
    UserPoolId: res.locals.tenantId,
    groupname: req.params.groupname ? req.params.groupname : null
  };

  Cognito_controller.getGroupInfo(params).then((group) => {
    res.json({
      status: 200,
      message: 'getGroupInfo fetched successfully',
      data: group,
    });
  });

})



// Get List Users In Group
router.get('/listUsersInGroup/:groupname', (req, res, next) => {

  console.log('***GET ListUsersInGroup CALLED***');

  let tenantId = res.locals.tenantId;

  const params = {
    UserPoolId: tenantId,
    groupname: req.params.groupname ? req.params.groupname : null
  }

  console.log('*** GET ListUsersInGroup CALLED ***' + JSON.stringify(params))

  Cognito_controller.listUsersInGroup(params).then((users) => {
    res.json({
      status: 200,
      message: 'List Of Users in Group fetched successfully',
      data: users,
    });
  });
});



// Delete User
router.post('/adminRemoveUserFromGroup', (req, res, next) => {
  console.log("adminRemoveUserFromGroup POST called")
  const params = {
    groupname: req.body.groupname ? req.body.groupname : null,
    username: req.body.username ? req.body.username : null,
    UserPoolId: res.locals.tenantId,
  };

  console.log("routes sdrtfgjh" + JSON.stringify(params))

  Cognito_controller.adminRemoveUserFromGroup(params).then((response) => {
    if (response) {
      console.log('remove user from group response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Removed From Group Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN User Removed From Group: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})

module.exports = router;